package com.dharmah

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager.widget.ViewPager
import androidx.viewpager.widget.ViewPager.OnPageChangeListener
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private val videos = ArrayList<String>()
    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        videos.add("android.resource://" + packageName + "/" + R.raw.video_one)
        videos.add("android.resource://" + packageName + "/" + R.raw.video_two)
        videos.add("android.resource://" + packageName + "/" + R.raw.video_three)
        var myAdapter = MoviesPagerAdapter(supportFragmentManager)
        viewPager.adapter = myAdapter
        viewPager.offscreenPageLimit = 1
        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                /*var fragment = myAdapter.getItem(position)
                (fragment as VideoFragment).resumePlay()
                for (i in 0 until videos.size) {
                    var fragment = myAdapter.getItem(position)
                    if (i != position)
                        (fragment as VideoFragment).pausePlay()
                }*/
            }
        })
    }

    inner class MoviesPagerAdapter(fragmentManager: FragmentManager) :
        FragmentStatePagerAdapter(fragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
        override fun getItem(position: Int): Fragment {
            return VideoFragment.newInstance(videos[position])
        }

        override fun getCount(): Int {
            return videos.size
        }
    }
}