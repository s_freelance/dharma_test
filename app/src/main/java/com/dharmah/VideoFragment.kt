package com.dharmah

import android.media.MediaPlayer.OnPreparedListener
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_video.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class VideoFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
        }
    }

    override fun onResume() {
        super.onResume()
//        resumePlay()
    }

    override fun onPause() {
        super.onPause()
//        pausePlay()
    }

    public fun resumePlay() {
        videoView.resume()
    }

    public fun pausePlay() {
        videoView?.pause()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_video, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val video: Uri = Uri.parse(param1)
        videoView.setVideoURI(video)
        videoView.start()
        videoView_blurred.setVideoURI(video)
        videoView_blurred.setOnPreparedListener { mp ->
            mp.setVolume(0f, 0f)
            mp.isLooping = true
        }
        videoView_blurred.start()
    }

    companion object {
        @JvmStatic
        fun newInstance(videoUri: String) = VideoFragment().apply {
            arguments = Bundle().apply {
                putString(ARG_PARAM1, videoUri)
            }
        }
    }
}